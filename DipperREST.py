import asyncio
# import code
from aiohttp import web
from collections import defaultdict
import json

current_status=defaultdict(str)
t_out=defaultdict(str)
resp={"status":""}

async def timer(connid,timeout,decr=1):
    "Coroutine to calculate and update the remaining time."

    current_status[connid]=timeout
    while timeout:
        timeout-=decr
        await asyncio.sleep(decr)
        if connid in t_out.keys():
            current_status[connid]=timeout
        else:
            return False
    del current_status[connid],t_out[connid]
    return True



async def handler(request):
    "Coroutine to handle new connection requests"

    qs = request.query
    try:
        connid=qs["connid"]
        timeout=int(qs["timeout"])
    
    except:
        resp["status"]="Parameters incorrect/missing."

    else:
        if connid in t_out.keys() or connid in current_status.keys():
            resp["status"]="Duplicate request."

        else:
            t_out[connid]=str(timeout)
            if not await timer(connid,timeout):
                resp["status"]="Killed"
            else:
                resp["status"]="ok"
    
    finally:
        return web.Response(body=json.dumps(resp),content_type="application/json")


async def stats(request):
    "Coroutine to return Server's current Stats"
    return web.Response(body=json.dumps(current_status),content_type="application/json")

async def killer(request):
    "Coroutine to kill running requests on demand"
    
    x=await request.post()
    try:
        connid=x["connid"]
    except:
        resp["status"]="Parameters missing"
    else:
        if connid in current_status.keys() and connid in t_out.keys():
            del t_out[connid],current_status[connid]
            resp["status"]="ok"
        else:
            resp["status"]="Invalid Connection ID: <{}>".format(connid)
    finally:
        return web.Response(body=json.dumps(resp),content_type="application/json")

async def init(loop):
    "Initial setting up of the server"
    app = web.Application()
    app.router.add_get('/api/request', handler)
    app.router.add_get('/serverStatus',stats)
    app.router.add_put('/api/kill',killer)
    return app

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    app=loop.run_until_complete(init(loop))
    web.run_app(app,host="127.0.0.1",port=8000)
    # code.interact(local=locals())